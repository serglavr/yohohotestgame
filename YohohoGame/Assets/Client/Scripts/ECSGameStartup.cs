using Client.Scripts.Components;
using Client.Scripts.ScriptableObjects;
using Client.Scripts.Systems;
using Leopotam.EcsLite;
using Leopotam.EcsLite.ExtendedSystems;
using TMPro;
using UnityEngine;

namespace Client.Scripts
{
    public class ECSGameStartup : MonoBehaviour
    {
        private EcsWorld ecsWorld;
        private IEcsSystems initSystems;
        private IEcsSystems updateSystems;
        private IEcsSystems fixedUpdateSystems;

        [SerializeField] private SettingsSO _settings;
        [SerializeField] private SpawningObject _spawnObject;
        [SerializeField] private Transform _parentForSpawned;
        [SerializeField] private FixedJoystick _joystick;
        [SerializeField] private Transform _objectsPlace;
        [SerializeField] private TextMeshProUGUI _scoreText;
        [SerializeField] private Transform[] _spawnPoints;

        private void Start()
        {
            ecsWorld = new EcsWorld();
            var gameData = new GameData();

            gameData.Settings = _settings;
            gameData.Joystick = _joystick;
            gameData.ObjectsPlace = _objectsPlace;
            gameData.ScoreText = _scoreText;

            gameData.SpawnPoints = _spawnPoints;
            gameData.SpawnObject = _spawnObject;
            gameData.ParentForSpawned = _parentForSpawned;

            initSystems = new EcsSystems(ecsWorld, gameData)
                .Add(new PlayerInitSystem())
                .Add(new SpawnerInitSystem())
                .Add(new AggregatorInitSystem());

            initSystems.Init();

            updateSystems = new EcsSystems(ecsWorld, gameData)
                .Add(new PlayerInputSystem())
                .Add(new PlayerHitSystem())
                .Add(new SpawnerBlockSystem())
                .Add(new SpawnerSpawnSystem())
                .DelHere<HitStartComponent>()
                .DelHere<HitFinishComponent>();

            updateSystems.Init();

            fixedUpdateSystems = new EcsSystems(ecsWorld, gameData)
                .Add(new PlayerMoveSystem())
                .Add(new PlayerPickUpSystem())
                .Add(new PlayerPickOffSystem())
                .Add(new MoveCarriedObjectsSystem());

            fixedUpdateSystems.Init();
        }

        private void Update()
        {
            updateSystems.Run();
        }

        private void FixedUpdate()
        {
            fixedUpdateSystems.Run();
        }

        private void OnDestroy()
        {
            initSystems.Destroy();
            updateSystems.Destroy();
            fixedUpdateSystems.Destroy();
            ecsWorld.Destroy();
        }
    }
}