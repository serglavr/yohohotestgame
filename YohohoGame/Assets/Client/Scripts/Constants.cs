﻿namespace Client.Scripts
{
    public static class Constants
    {
        public static class Tags
        {
            public const string SpawnerTag = "SpawnerTable";
            public const string AggregatorTag = "AggregatorTable";
        }
    }
}