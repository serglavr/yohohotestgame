﻿using UnityEngine;

namespace Client.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "Settings")]
    public class SettingsSO : ScriptableObject
    {
        public float PlayerSpeed = 4f;
        public float RotationSpeed = 8f;
        public float ObjectLerpSpeed = 15f;
        public int MaxObjectsCarried = 3;
        public float SpawnTime = 2f;
    }
}