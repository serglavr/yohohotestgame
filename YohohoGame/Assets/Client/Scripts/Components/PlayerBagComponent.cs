﻿using System.Collections.Generic;
using UnityEngine;

namespace Client.Scripts.Components
{
    public struct PlayerBagComponent
    {
        public List<Transform> Objects;
    }
}