﻿using UnityEngine;

namespace Client.Scripts.Components
{
    public struct PlayerInputComponent
    {
        public Vector3 Position;
        public Vector3 TargetPosition;
        public Vector3 Direction;
    }
}