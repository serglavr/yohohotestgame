﻿using UnityEngine;

namespace Client.Scripts.Components
{
    public struct SpawnerComponent
    {
        public int LastSpawnedIndex;

        public Transform[] SpawnedObjects;
        public bool[] Spawned;
        //public Vector3 SpawnerVelocity;
    }
}