﻿using UnityEngine;

namespace Client.Scripts.Components
{
    public struct HitFinishComponent
    {
        public GameObject Other;
    }
}