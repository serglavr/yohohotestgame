﻿using UnityEngine;

namespace Client.Scripts.Components
{
    public struct PlayerComponent
    {
        public Transform PlayerTransform;
        public float RotationSpeed;
        public float PlayerSpeed;
        public int MaxObjectsCarried;
        public Animator Animator;
    }
}