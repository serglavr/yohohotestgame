﻿using UnityEngine;

namespace Client.Scripts.Components
{
    public struct HitStartComponent
    {
        public GameObject Other;
    }
}
