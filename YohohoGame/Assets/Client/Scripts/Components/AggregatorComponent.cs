﻿using UnityEngine;

namespace Client.Scripts.Components
{
    public struct AggregatorComponent
    {
        public Transform AggregatorTransform;
        public Vector3 LastAggregatePosition;
        public int Score;
    }
}