﻿using Client.Scripts.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace Client.Scripts
{
    public class CollisionCheckerView : MonoBehaviour
    {
        public EcsWorld EcsWorld { get; set; }

        private void OnTriggerEnter(Collider other)
        {
            var hit = EcsWorld.NewEntity();

            var hitStartPool = EcsWorld.GetPool<HitStartComponent>();
            hitStartPool.Add(hit);
            ref var hitComponent = ref hitStartPool.Get(hit);

            hitComponent.Other = other.gameObject;
        }

        private void OnTriggerExit(Collider other)
        {
            var hit = EcsWorld.NewEntity();

            var hitFinishPool = EcsWorld.GetPool<HitFinishComponent>();
            hitFinishPool.Add(hit);
            ref var hitComponent = ref hitFinishPool.Get(hit);

            hitComponent.Other = other.gameObject;
        }
    }
}