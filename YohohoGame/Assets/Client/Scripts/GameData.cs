﻿using Client.Scripts.ScriptableObjects;
using TMPro;
using UnityEngine;

namespace Client.Scripts
{
    public class GameData
    {
        public SettingsSO Settings;
        public FixedJoystick Joystick;
        public Transform ObjectsPlace;
        public TextMeshProUGUI ScoreText;

        public SpawningObject SpawnObject;
        public Transform[] SpawnPoints;
        public Transform ParentForSpawned;
    }
}