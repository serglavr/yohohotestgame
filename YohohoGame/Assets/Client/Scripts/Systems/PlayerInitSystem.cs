﻿using System.Collections.Generic;
using Client.Scripts.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace Client.Scripts.Systems
{
    public class PlayerInitSystem : IEcsInitSystem
    {
        public void Init(IEcsSystems systems)
        {
            var ecsWorld = systems.GetWorld();
            var gameData = systems.GetShared<GameData>();

            var playerEntity = ecsWorld.NewEntity();

            var playerPool = ecsWorld.GetPool<PlayerComponent>();
            playerPool.Add(playerEntity);
            ref var playerComponent = ref playerPool.Get(playerEntity);
            var playerInputPool = ecsWorld.GetPool<PlayerInputComponent>();
            playerInputPool.Add(playerEntity);

            var playerBagPool = ecsWorld.GetPool<PlayerBagComponent>();
            playerBagPool.Add(playerEntity);
            ref var playerBag = ref playerBagPool.Get(playerEntity);
            playerBag.Objects = new List<Transform>();
            playerBag.Objects.Add(gameData.ObjectsPlace);

            var playerGO = GameObject.FindGameObjectWithTag("Player");
            playerGO.GetComponentInChildren<CollisionCheckerView>().EcsWorld = ecsWorld;
            playerComponent.RotationSpeed = gameData.Settings.RotationSpeed;
            playerComponent.PlayerSpeed = gameData.Settings.PlayerSpeed;
            playerComponent.MaxObjectsCarried = gameData.Settings.MaxObjectsCarried;
            playerComponent.PlayerTransform = playerGO.transform;
            playerComponent.Animator = playerGO.GetComponent<Animator>();
        }
    }
}