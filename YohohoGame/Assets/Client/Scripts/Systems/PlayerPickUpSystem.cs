﻿using System;
using System.Linq;
using Client.Scripts.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace Client.Scripts.Systems
{
    public class PlayerPickUpSystem : IEcsRunSystem
    {
        public void Run(IEcsSystems systems)
        {
            var bagPool = systems.GetWorld().GetPool<PlayerBagComponent>();
            var playerFilter = systems.GetWorld().Filter<PlayerComponent>().Inc<PlayerCanPickUpComponent>().End();
            var playerCanPickUpPool = systems.GetWorld().GetPool<PlayerCanPickUpComponent>();
            var playerPool = systems.GetWorld().GetPool<PlayerComponent>();
            var spawnerPool = systems.GetWorld().GetPool<SpawnerComponent>();
            var spawnerCanGivePool = systems.GetWorld().GetPool<SpawnerCanGiveObjectsComponent>();
            var spawnerCanGiveFilter = systems.GetWorld().Filter<SpawnerCanGiveObjectsComponent>().End();
            var spawnerIsNearPool = systems.GetWorld().GetPool<SpawnerIsNearComponent>();
            var spawnerIsNearFilter = systems.GetWorld().Filter<SpawnerIsNearComponent>().End();

            foreach (var entity in playerFilter)
            {
                ref var playerComponent = ref playerPool.Get(entity);

                var isPickedSomething = false;

                if (playerCanPickUpPool.Has(entity))
                {
                    ref var playerBag = ref bagPool.Get(entity);
                    if (playerBag.Objects.Count < playerComponent.MaxObjectsCarried + 1)
                    {
                        foreach (var spawnIsNearE in spawnerIsNearFilter)
                        {
                            foreach (var spawnCanGiveE in spawnerCanGiveFilter)
                            {
                                if (spawnerIsNearPool.Has(spawnIsNearE) && spawnerCanGivePool.Has(spawnCanGiveE))
                                {
                                    ref var spawner = ref spawnerPool.Get(spawnCanGiveE);
                                    TryToTakeObject(spawner, playerBag, playerComponent, out var isPicked);

                                    if (spawner.Spawned.Count(x => x) < 1)
                                    {
                                        spawnerCanGivePool.Del(entity);
                                    }

                                    if (!isPickedSomething)
                                    {
                                        isPickedSomething = isPicked;
                                    }
                                }
                            }
                        }
                    }
                }

                if (isPickedSomething)
                {
                    TryToPlayAnimation(playerComponent);
                }
            }
        }

        private void TryToTakeObject(SpawnerComponent spawnIsNearE, PlayerBagComponent playerBag,
            PlayerComponent playerComponent, out bool isPicked)
        {
            if (playerBag.Objects.Count < playerComponent.MaxObjectsCarried + 1)
            {
                var spawnedObjectIndex =
                    Array.FindIndex(spawnIsNearE.Spawned, x => spawnIsNearE.Spawned.First(y => y) == x);
                var spawnedObject = spawnIsNearE.SpawnedObjects[spawnedObjectIndex];
                playerBag.Objects.Add(spawnedObject);
                spawnedObject.SetParent(playerComponent.PlayerTransform);
                spawnIsNearE.Spawned[spawnedObjectIndex] = false;
                isPicked = true;
                return;
            }

            isPicked = false;
        }

        private void TryToPlayAnimation(PlayerComponent playerComponent)
        {
            playerComponent.Animator.enabled=false;
            playerComponent.Animator.enabled=true;
            
            playerComponent.Animator.Play("Crouch_Down");
            
            var waitTime = 0f;
            while (waitTime < 3f)
            {
                waitTime += Time.fixedDeltaTime;
            }
            
            playerComponent.Animator.Play("Crouch_Up");
        }
    }
}