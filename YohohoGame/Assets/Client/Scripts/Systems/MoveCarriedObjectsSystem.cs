﻿using System.Linq;
using Client.Scripts.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace Client.Scripts.Systems
{
    public class MoveCarriedObjectsSystem : IEcsRunSystem

    {
        public void Run(IEcsSystems systems)
        {
            var bagPool = systems.GetWorld().GetPool<PlayerBagComponent>();
            var playerFilter = systems.GetWorld().Filter<PlayerComponent>().End();
            var gameData = systems.GetShared<GameData>();

            foreach (var playerEntity in playerFilter)
            {
                ref var playerBag = ref bagPool.Get(playerEntity);

                if (playerBag.Objects.Count > 1)
                {
                    for (var i = 1; i < playerBag.Objects.Count; i++)
                    {
                        var firstObject = playerBag.Objects.ElementAt(i - 1);
                        var secondObject = playerBag.Objects.ElementAt(i);

                        var foPos = firstObject.position;
                        var soPos = secondObject.position;

                        secondObject.position = new Vector3(
                            Mathf.Lerp(soPos.x, foPos.x, Time.fixedDeltaTime * gameData.Settings.ObjectLerpSpeed),
                            Mathf.Lerp(soPos.y, foPos.y + 0.33f, Time.fixedDeltaTime * gameData.Settings.ObjectLerpSpeed),
                            foPos.z
                        );
                    }
                }
            }
        }
    }
}