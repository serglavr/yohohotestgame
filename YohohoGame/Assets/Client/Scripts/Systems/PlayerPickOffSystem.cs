﻿using System.Linq;
using Client.Scripts.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace Client.Scripts.Systems
{
    public class PlayerPickOffSystem : IEcsRunSystem
    {
        private const float ObjectHeight = 0.42f;

        public void Run(IEcsSystems systems)
        {
            var bagPool = systems.GetWorld().GetPool<PlayerBagComponent>();
            var playerFilter = systems.GetWorld().Filter<PlayerComponent>().Inc<PlayerCanPickOffComponent>().End();
            var playerCanPickOffPool = systems.GetWorld().GetPool<PlayerCanPickOffComponent>();
            var playerPool = systems.GetWorld().GetPool<PlayerComponent>();
            var gameData = systems.GetShared<GameData>();
            var aggregatorPool = systems.GetWorld().GetPool<AggregatorComponent>();
            var aggregatorCanTakePool = systems.GetWorld().GetPool<AggregatorCanTakeObjectsComponent>();
            var aggregatorCanTakeFilter = systems.GetWorld().Filter<AggregatorCanTakeObjectsComponent>().End();
            var aggregatorIsNearPool = systems.GetWorld().GetPool<AggregatorIsNearComponent>();
            var aggregatorIsNearFilter = systems.GetWorld().Filter<AggregatorIsNearComponent>().End();

            foreach (var entity in playerFilter)
            {
                ref var playerComponent = ref playerPool.Get(entity);
                
                var isGivenSomething = false;

                if (playerCanPickOffPool.Has(entity))
                {
                    ref var playerBag = ref bagPool.Get(entity);
                    if (playerBag.Objects.Count > 1)
                    {
                        foreach (var aggregatorIsNearE in aggregatorIsNearFilter)
                        {
                            foreach (var aggregatorCanTakeE in aggregatorCanTakeFilter)
                            {
                                if (aggregatorIsNearPool.Has(aggregatorIsNearE) &&
                                    aggregatorCanTakePool.Has(aggregatorCanTakeE))
                                {
                                    ref var aggregator = ref aggregatorPool.Get(aggregatorCanTakeE);
                                    TryToGiveObject(aggregator, playerBag, out var isGiven);

                                    if (isGiven)
                                    {
                                        aggregator.LastAggregatePosition.y += ObjectHeight;
                                        aggregator.Score++;
                                        gameData.ScoreText.text = aggregator.Score.ToString();
                                    }

                                    if (!isGivenSomething)
                                    {
                                        isGivenSomething = isGiven;
                                    }
                                }
                            }
                        }
                    }
                }
                
                if (isGivenSomething)
                {
                    TryToPlayAnimation(playerComponent);
                }
            }
        }

        private void TryToGiveObject(AggregatorComponent aggregator, PlayerBagComponent playerBag,
            out bool isGiven)
        {
            isGiven = false;

            if (playerBag.Objects.Count > 1)
            {
                playerBag.Objects.Last().parent = aggregator.AggregatorTransform.parent;
                playerBag.Objects.Last().position = aggregator.LastAggregatePosition;
                playerBag.Objects.RemoveAt(playerBag.Objects.Count - 1);

                isGiven = true;
            }
        }
        
        private void TryToPlayAnimation(PlayerComponent playerComponent)
        {
            playerComponent.Animator.enabled=false;
            playerComponent.Animator.enabled=true;
            
            playerComponent.Animator.Play("Crouch_Down");
            
            var waitTime = 0f;
            while (waitTime < 3f)
            {
                waitTime += Time.fixedDeltaTime;
            }
            
            playerComponent.Animator.Play("Crouch_Up");
        }
    }
}