﻿using Client.Scripts.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace Client.Scripts.Systems
{
    public class SpawnerInitSystem : IEcsInitSystem
    {
        public void Init(IEcsSystems systems)
        {
            var ecsWorld = systems.GetWorld();
            var gameData = systems.GetShared<GameData>();

            var spawnerEntity = ecsWorld.NewEntity();

            var spawnerPool = ecsWorld.GetPool<SpawnerComponent>();
            spawnerPool.Add(spawnerEntity);
            ref var spawnerComponent = ref spawnerPool.Get(spawnerEntity);

            var spawnerGO = GameObject.FindGameObjectWithTag(Constants.Tags.SpawnerTag);
            spawnerGO.GetComponentInChildren<CollisionCheckerView>().EcsWorld = ecsWorld;
            spawnerComponent.LastSpawnedIndex = 0;
            spawnerComponent.Spawned = new bool[gameData.SpawnPoints.Length];
            spawnerComponent.SpawnedObjects = new Transform[gameData.SpawnPoints.Length];
        }
    }
}