﻿using System.Linq;
using Client.Scripts.Components;
using Leopotam.EcsLite;
using Unity.Mathematics;
using UnityEngine;

namespace Client.Scripts.Systems
{
    public class SpawnerSpawnSystem : IEcsRunSystem
    {
        public void Run(IEcsSystems systems)
        {
            var gameData = systems.GetShared<GameData>();
            var spawnerBlockedFilter = systems.GetWorld().Filter<SpawnerComponent>().Exc<SpawnerBlockComponent>().End();
            var spawnerPool = systems.GetWorld().GetPool<SpawnerComponent>();
            var blockPool = systems.GetWorld().GetPool<SpawnerBlockComponent>();
            var spawnerCanGiveObjectsPool = systems.GetWorld().GetPool<SpawnerCanGiveObjectsComponent>();

            foreach (var entity in spawnerBlockedFilter)
            {
                ref var spawnerComponent = ref spawnerPool.Get(entity);


                var lastSpawnedIndex = spawnerComponent.LastSpawnedIndex;
                if (spawnerComponent.Spawned[lastSpawnedIndex])
                {
                    if (lastSpawnedIndex < gameData.SpawnPoints.Length - 1)
                    {
                        spawnerComponent.LastSpawnedIndex++;
                    }
                    else
                    {
                        spawnerComponent.LastSpawnedIndex = 0;
                    }
                }
                else
                {
                    spawnerComponent = SpawnObject(gameData, lastSpawnedIndex, spawnerComponent);
                }

                blockPool.Add(entity);
                ref var blockComponent = ref blockPool.Get(entity);
                blockComponent.Timer = gameData.Settings.SpawnTime;

                if (spawnerComponent.Spawned.Count(x => x) > 0)
                {
                    if (!spawnerCanGiveObjectsPool.Has(entity))
                    {
                        spawnerCanGiveObjectsPool.Add(entity);
                    }
                }
            }
        }

        private SpawnerComponent SpawnObject(GameData gameData, int lastSpawnedIndex, SpawnerComponent spawnerComponent)
        {
            var obj = Object.Instantiate(gameData.SpawnObject,
                new Vector3(gameData.SpawnPoints[lastSpawnedIndex].position.x,
                    gameData.SpawnPoints[lastSpawnedIndex].position.y + 0.2f,
                    gameData.SpawnPoints[lastSpawnedIndex].position.z), quaternion.identity,
                gameData.ParentForSpawned);

            spawnerComponent.Spawned[lastSpawnedIndex] = true;
            spawnerComponent.SpawnedObjects[lastSpawnedIndex] = obj.transform;
            obj.Index = lastSpawnedIndex;

            if (lastSpawnedIndex < gameData.SpawnPoints.Length - 1)
            {
                spawnerComponent.LastSpawnedIndex++;
            }
            else
            {
                spawnerComponent.LastSpawnedIndex = 0;
            }

            return spawnerComponent;
        }
    }
}