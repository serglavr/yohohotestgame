﻿using Client.Scripts.Components;
using Leopotam.EcsLite;

namespace Client.Scripts.Systems
{
    public class PlayerHitSystem : IEcsRunSystem
    {
        public void Run(IEcsSystems systems)
        {
            var hitStartFilter = systems.GetWorld().Filter<HitStartComponent>().End();
            var hitStartPool = systems.GetWorld().GetPool<HitStartComponent>();

            var hitFinishFilter = systems.GetWorld().Filter<HitFinishComponent>().End();
            var hitFinishPool = systems.GetWorld().GetPool<HitFinishComponent>();

            var playerFilter = systems.GetWorld().Filter<PlayerComponent>().Inc<PlayerBagComponent>().End();

            var playerCanPickUpPool = systems.GetWorld().GetPool<PlayerCanPickUpComponent>();
            var spawnerIsNearPool = systems.GetWorld().GetPool<SpawnerIsNearComponent>();

            var playerCanPickOffPool = systems.GetWorld().GetPool<PlayerCanPickOffComponent>();
            var aggregatorIsNearPool = systems.GetWorld().GetPool<AggregatorIsNearComponent>();

            foreach (var hitEntity in hitStartFilter)
            {
                ref var hitStartComponent = ref hitStartPool.Get(hitEntity);

                foreach (var playerEntity in playerFilter)
                {
                    CheckSpawnerTagTriggerEntered(hitStartComponent, playerCanPickUpPool, playerEntity,
                        spawnerIsNearPool, hitEntity);

                    CheckAggregatorTagTriggerEntered(hitStartComponent, playerCanPickOffPool, playerEntity,
                        aggregatorIsNearPool, hitEntity);
                }
            }

            foreach (var hitEntity in hitFinishFilter)
            {
                ref var hitFinishComponent = ref hitFinishPool.Get(hitEntity);

                foreach (var playerEntity in playerFilter)
                {
                    CheckSpawnerTagTriggerExit(hitFinishComponent, playerCanPickUpPool, playerEntity,
                        spawnerIsNearPool, hitEntity);

                    CheckAggregatorTagTriggerExit(hitFinishComponent, playerCanPickOffPool, playerEntity,
                        aggregatorIsNearPool, hitEntity);
                }
            }
        }

        private void CheckSpawnerTagTriggerExit(HitFinishComponent hitFinishComponent,
            EcsPool<PlayerCanPickUpComponent> playerCanPickUpPool,
            int playerEntity, EcsPool<SpawnerIsNearComponent> spawnerIsNearPool, int hitEntity)
        {
            if (hitFinishComponent.Other.CompareTag(Constants.Tags.SpawnerTag))
            {
                if (playerCanPickUpPool.Has(playerEntity))
                {
                    playerCanPickUpPool.Del(playerEntity);
                }

                if (spawnerIsNearPool.Has(hitEntity))
                {
                    spawnerIsNearPool.Del(hitEntity);
                }
            }
        }

        private void CheckAggregatorTagTriggerExit(HitFinishComponent hitFinishComponent,
            EcsPool<PlayerCanPickOffComponent> playerCanPickOffPool,
            int playerEntity, EcsPool<AggregatorIsNearComponent> aggregatorIsNearPool, int hitEntity)
        {
            if (hitFinishComponent.Other.CompareTag(Constants.Tags.AggregatorTag))
            {
                if (playerCanPickOffPool.Has(playerEntity))
                {
                    playerCanPickOffPool.Del(playerEntity);
                }

                if (aggregatorIsNearPool.Has(hitEntity))
                {
                    aggregatorIsNearPool.Del(hitEntity);
                }
            }
        }

        private void CheckSpawnerTagTriggerEntered(HitStartComponent hitStartComponent,
            EcsPool<PlayerCanPickUpComponent> playerCanPickUpPool, int playerEntity,
            EcsPool<SpawnerIsNearComponent> spawnerIsNearPool, int hitEntity)
        {
            if (hitStartComponent.Other.CompareTag(Constants.Tags.SpawnerTag))
            {
                if (!playerCanPickUpPool.Has(playerEntity))
                {
                    playerCanPickUpPool.Add(playerEntity);
                }

                if (!spawnerIsNearPool.Has(hitEntity))
                {
                    spawnerIsNearPool.Add(hitEntity);
                }
            }
        }

        private void CheckAggregatorTagTriggerEntered(HitStartComponent hitStartComponent,
            EcsPool<PlayerCanPickOffComponent> playerCanPickOffPool,
            int playerEntity, EcsPool<AggregatorIsNearComponent> aggregatorIsNearPool, int hitEntity)
        {
            if (hitStartComponent.Other.CompareTag(Constants.Tags.AggregatorTag))
            {
                if (!playerCanPickOffPool.Has(playerEntity))
                {
                    playerCanPickOffPool.Add(playerEntity);
                }

                if (!aggregatorIsNearPool.Has(hitEntity))
                {
                    aggregatorIsNearPool.Add(hitEntity);
                }
            }
        }
    }
}