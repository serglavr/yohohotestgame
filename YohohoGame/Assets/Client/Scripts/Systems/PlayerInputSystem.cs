﻿using Client.Scripts.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace Client.Scripts.Systems
{
    public class PlayerInputSystem : IEcsRunSystem
    {
        public void Run(IEcsSystems ecsSystems)
        {
            var inputFilter = ecsSystems.GetWorld().Filter<PlayerInputComponent>().End();
            var playerFilter = ecsSystems.GetWorld().Filter<PlayerComponent>().End();
            var playerInputPool = ecsSystems.GetWorld().GetPool<PlayerInputComponent>();
            var playerPool = ecsSystems.GetWorld().GetPool<PlayerComponent>();
            var gameData = ecsSystems.GetShared<GameData>();

            foreach (var entity in inputFilter)
            {
                ref var playerInputComponent = ref playerInputPool.Get(entity);

                foreach (var playerE in playerFilter)
                {
                    ref var playerComponent = ref playerPool.Get(playerE);
                    
                    playerInputComponent.Position = playerComponent.PlayerTransform.position;
                    playerInputComponent.Direction = Vector3.forward * gameData.Joystick.Vertical +
                                                     Vector3.right * gameData.Joystick.Horizontal;

                    var moveX = playerInputComponent.Position.x + playerInputComponent.Direction.x;
                    var moveZ = playerInputComponent.Position.z + playerInputComponent.Direction.z;

                    playerInputComponent.TargetPosition = new Vector3(moveX, Vector3.forward.y, moveZ);
                }
            }
        }
    }
}