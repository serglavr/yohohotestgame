﻿using System;
using Client.Scripts.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace Client.Scripts.Systems
{
    public class PlayerMoveSystem : IEcsRunSystem
    {
        public void Run(IEcsSystems systems)
        {
            var filter = systems.GetWorld().Filter<PlayerComponent>().Inc<PlayerInputComponent>().End();
            var playerPool = systems.GetWorld().GetPool<PlayerComponent>();
            var playerInputPool = systems.GetWorld().GetPool<PlayerInputComponent>();

            foreach (var entity in filter)
            {
                ref var playerComponent = ref playerPool.Get(entity);
                ref var playerInputComponent = ref playerInputPool.Get(entity);

                var tr = playerComponent.PlayerTransform;

                //Debug.Log("xxx: "+ (playerInputComponent.TargetPosition.x - playerInputComponent.Position.x));
                //Debug.Log("yyy: "+ (playerInputComponent.TargetPosition.y - playerInputComponent.Position.y));
                //Debug.Log("zzz: "+ (playerInputComponent.TargetPosition.z - playerInputComponent.Position.z));

                AnimPlayer(playerInputComponent, playerComponent);
                
                /*if (playerInputComponent.TargetPosition.x - playerInputComponent.Position.x < 0f &&
                    playerInputComponent.TargetPosition.y - playerInputComponent.Position.y < 0f &&
                    playerInputComponent.TargetPosition.z - playerInputComponent.Position.z< 0f)
                {
                    
                }*/

                playerComponent.PlayerTransform.position = Vector3.MoveTowards(playerInputComponent.Position,
                    playerInputComponent.TargetPosition,
                    playerComponent.PlayerSpeed * Time.fixedDeltaTime);

                playerComponent.PlayerTransform.transform.forward = Vector3.Slerp(tr.forward,
                    playerInputComponent.Direction, Time.fixedDeltaTime * playerComponent.RotationSpeed);
            }
        }

        private void AnimPlayer(PlayerInputComponent playerInputComponent, PlayerComponent playerComponent)
        {
            var absDiffX = Math.Abs(playerInputComponent.TargetPosition.x - playerInputComponent.Position.x);
            var absDiffZ = Math.Abs(playerInputComponent.TargetPosition.z - playerInputComponent.Position.z);
            
            
            if (absDiffX is > 0.05f and <= 0.25f || absDiffZ is > 0.05f and <= 0.25f)
            {
                if (playerComponent.Animator.GetBool("IsRunning"))
                {
                    playerComponent.Animator.SetBool("IsRunning", false);
                }
                
                if (!playerComponent.Animator.GetBool("IsWalking"))
                {
                    playerComponent.Animator.SetBool("IsWalking", true);
                }

                return;
            }
            
            if (absDiffX > 0.25f || absDiffZ > 0.25f)
            {
                if (!playerComponent.Animator.GetBool("IsRunning"))
                {
                    playerComponent.Animator.SetBool("IsRunning", true);
                }
                
                if (playerComponent.Animator.GetBool("IsWalking"))
                {
                    playerComponent.Animator.SetBool("IsWalking", false);
                }

                return;
            }
            
            if (absDiffX <= 0.05f && absDiffZ <= 0.05f)
            {
                if (playerComponent.Animator.GetBool("IsRunning"))
                {
                    playerComponent.Animator.SetBool("IsRunning", false);
                }
                
                if (playerComponent.Animator.GetBool("IsWalking"))
                {
                    playerComponent.Animator.SetBool("IsWalking", false);
                }
            }
        }
    }
}