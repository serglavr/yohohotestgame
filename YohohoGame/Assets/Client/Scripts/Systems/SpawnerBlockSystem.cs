﻿using Client.Scripts.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace Client.Scripts.Systems
{
    public class SpawnerBlockSystem : IEcsRunSystem
    {
        public void Run(IEcsSystems systems)
        {
            var spawnerBlockedFilter = systems.GetWorld().Filter<SpawnerComponent>().Inc<SpawnerBlockComponent>().End();
            var blockPool = systems.GetWorld().GetPool<SpawnerBlockComponent>();

            foreach (var entity in spawnerBlockedFilter)
            {
                ref var spawnerComponent = ref blockPool.Get(entity);
                spawnerComponent.Timer -= Time.deltaTime;

                if (spawnerComponent.Timer <= 0)
                {
                    blockPool.Del(entity);
                }
            }
        }
    }
}