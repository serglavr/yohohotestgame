﻿using Client.Scripts.Components;
using Leopotam.EcsLite;
using UnityEngine;

namespace Client.Scripts.Systems
{
    public class AggregatorInitSystem : IEcsInitSystem
    {
        public void Init(IEcsSystems systems)
        {
            var ecsWorld = systems.GetWorld();

            var aggregatorEntity = ecsWorld.NewEntity();

            var aggregatorPool = ecsWorld.GetPool<AggregatorComponent>();
            aggregatorPool.Add(aggregatorEntity);
            ref var aggregatorComponent = ref aggregatorPool.Get(aggregatorEntity);

            var aggregatorCanTakeObjectsPool = ecsWorld.GetPool<AggregatorCanTakeObjectsComponent>();
            aggregatorCanTakeObjectsPool.Add(aggregatorEntity);

            var aggregatorGO = GameObject.FindGameObjectWithTag(Constants.Tags.AggregatorTag);
            aggregatorGO.GetComponentInChildren<CollisionCheckerView>().EcsWorld = ecsWorld;
            aggregatorComponent.AggregatorTransform = aggregatorGO.transform;
            aggregatorComponent.Score = 0;
            aggregatorComponent.LastAggregatePosition = aggregatorGO.transform.position;
            aggregatorComponent.LastAggregatePosition.y += 0.21f;
        }
    }
}